# Mastodon Tray
-------------------------------
A mastodon notification tray.

## Dependancies
- A working go installation
- A linux desktop

## How to use
- run the install script (not as root)
- edit the config file as shown below

## Config token
- must only be done once
- additional values will be added after first run
- ~/.config/mastodon-tray/conf.json
```
{
  "Instance": "https://whomst.dog",
  "Username": "<fediverse @ handle>",
  "Password": "..."
}
```

