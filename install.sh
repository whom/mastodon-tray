#!/bin/bash
go get github.com/getlantern/systray
go get github.com/McKael/madon
go get github.com/0xAX/notificator
go build 

mkdir ~/.config/mastodon-tray/
cp icon.png ~/.config/mastodon-tray/
echo "you may now move the compiled binary wherever you would like to"
echo "be sure to edit ~/.config/mastodon-tray/conf.json"
