package main

import (
  "os"
  "fmt"
  "time"
  "os/user"
  "io/ioutil"
  "encoding/json"
  "github.com/getlantern/systray"
  "github.com/McKael/madon"
  "github.com/0xAX/notificator"
)

type Config struct {
  Instance string `json:"Instance"`
  Username string `json:"Username"`
  Password string `json:"Password"`
  ClientName string `json:"ClientName"`
  ClientID string `json:"ClientID"`
  ClientSecret string `json:"ClientSecret"`
}

var notify *notificator.Notificator
var client *madon.Client
var config Config
var needsNew bool = false

func main() {
  notify = notificator.New(notificator.Options{
    DefaultIcon: "icon.png",
    AppName: "Mastodon Tray",
  })

  go systray.Run(onReady, onExit)

  LoadConfig()
  switch {
  case config.Instance == "":
    notify.Push("Mastodon Tray", "Please set JSON value 'Instance' in ~/.config/mastodon-tray.json", "", notificator.UR_NORMAL)

  case config.Username == "":
    notify.Push("Mastodon Tray", "Please set JSON value 'Username' in ~/.config/mastodon-tray.json", "", notificator.UR_NORMAL)

  case config.Password == "":
    notify.Push("Mastodon Tray", "Please set JSON value 'Password' in ~/.config/mastodon-tray.json", "", notificator.UR_NORMAL)

  case config.ClientName == "":
    config.ClientName = "Mastodon-Tray"

  case config.ClientSecret == "" || config.ClientID == "":
    needsNew = true
  }

  if needsNew {
    client = AuthNew()

  } else {
    var err error
    client, err = madon.RestoreApp(config.ClientName, config.Instance, config.ClientID, config.ClientSecret, nil)

    if err != nil {
      notify.Push("Mastodon Tray", "Error reloading mastodon app data: " + err.Error(), "", notificator.UR_NORMAL)
    }
  }

  fmt.Println("[+] Authenticating")
  err := client.LoginBasic(config.Username, config.Password, []string{"read"})

  if err != nil {
    fmt.Println("[-] Auth Fail")
    notify.Push("Mastodon Tray", "Authentication Error: " + err.Error(), "", notificator.UR_NORMAL)

  } else {
    fmt.Println("[+] Auth Succeed")
    notify.Push("Mastodon Tray", "logged in", "", notificator.UR_NORMAL)
  }

  var LastID int64 = -1
  var notes []madon.Notification
  //var err error 
  // we are reusing the err previously defined in the client.LoginBasic() call above

  for true {
    if LastID > 0{
      notes, err = client.GetNotifications([]string{}, &madon.LimitParams{SinceID: LastID})

    } else {
      notes, err = client.GetNotifications([]string{}, &madon.LimitParams{Limit: 5})
    }

    if err != nil {
      fmt.Println("[-] Notification Error: " + err.Error())

    } else {
      for _, i := range notes {
        notifyStatus(&i)

	if i.ID > LastID {
	  LastID = i.ID
	}
      }
    }

    notes = nil
    time.Sleep(60 * 5 * time.Second)
  }

} // END MAIN

func LoadConfig() {
  u, _ := user.Current()
  cfile, err := os.Open(u.HomeDir + "/.config/mastodon-tray/conf.json")
  defer cfile.Close()

  if err != nil {
    fmt.Println("[-] Error opening config file: " + err.Error())
  }

  parser := json.NewDecoder(cfile)
  parser.Decode(&config)
}

func WriteConfig() {
  fmt.Println("[+] Writing back config file")

  cdata, _ :=json.Marshal(config)
  u, _ := user.Current()
  writeError := ioutil.WriteFile(u.HomeDir + "/.config/mastodon-tray/conf.json", cdata, 0644)

  if writeError != nil {
    fmt.Println("[-] Error writing config: " + writeError.Error())
  }
}

func AuthNew() *madon.Client {
  fmt.Println("[+] Registering New Application")

  app, err := madon.NewApp(config.ClientName, "https://gitlab.com/whom/mastodon-tray", []string{"read"},  "", config.Instance)

  if err != nil {
    notify.Push("Mastodon Tray", "Error registering with server: " + err.Error(), "", notificator.UR_NORMAL)
  }

  config.ClientSecret = app.Secret
  config.ClientID = app.ID

  return app
}

func notifyStatus(arg *madon.Notification) {
  notify.Push(arg.Type + " from " + arg.Account.Username, arg.Status.Content, "", notificator.UR_NORMAL)
}

func onReady() {
  u, _ := user.Current()
  file, _ := os.Open(u.HomeDir + "/.config/mastodon-tray/icon.png")
  bytes, _ := ioutil.ReadAll(file)

  systray.SetIcon(bytes)
  systray.SetTitle("Mastodon Tray")
  systray.SetTooltip("Mastodon Tray Notification Daemon")
  mQuit :=systray.AddMenuItem("Exit", "Exit mastodon tray")

  go func() {
    <-mQuit.ClickedCh
    systray.Quit()
  }()
}

func onExit() {
  WriteConfig()
  notify.Push("Mastodon Tray", "Logged out", "", notificator.UR_NORMAL)
  os.Exit(0)
}
